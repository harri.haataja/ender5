# Ender5

Ender 5 parts, mods or other things

The duetcover files are a little cover that attaches to the front bottom frame
right in front of the control electronics.
Since I've changed my motherboard to a Duet 2 Wifi, I had to cut some new holes
there and they aren't the most attractive thing.
This cover should hide them and protect them from contact without blocking
the wireless signal.
I don't normally need access to the card or USB, but the cover can be removed
if needed.
There is also a small hole to let LED lights show through.
I'll probably use something translucent to plug it, though it could also serve
as an extra air ingress.

Flange and sector are parts for a filament spool holder/case experiment.

Thermo2020 files are a quick model for mounting a cheap thermometer on the top
rail of the printer.
This way I can get a (relative) reading of the ambient temperature in the space
where the printer is.


## Upgrades

* [Cable protection for Ender-5](https://www.thingiverse.com/thing:3354725)
* [LM2596 Housing&Cover](https://www.thingiverse.com/thing:3384103)
* [Ender-5 Bed cable Strain Relief](https://www.thingiverse.com/thing:3443100)
* Bed supports

